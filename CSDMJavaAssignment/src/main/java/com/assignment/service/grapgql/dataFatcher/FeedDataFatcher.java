
package com.assignment.service.grapgql.dataFatcher;

import com.assignment.model.Feed;
import com.assignment.repository.FeedRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FeedDataFatcher implements DataFetcher<List<Feed>>{

    @Autowired
    FeedRepository feedRepository;
    
    @Override
    public List<Feed> get(DataFetchingEnvironment dfe) {
        return feedRepository.findAll();
    }
    
}
