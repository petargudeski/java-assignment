
package com.assignment.service.grapgql.dataFatcher;

import com.assignment.model.Feed;
import com.assignment.repository.FeedRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OneFeedDataFatcher implements DataFetcher<Feed>{

    @Autowired
    FeedRepository feedRepository;

    @Override
    public Feed get(DataFetchingEnvironment dfe) {
        Long id = dfe.getArgument("id"); 
        return feedRepository.findOne(id);
    }
}
