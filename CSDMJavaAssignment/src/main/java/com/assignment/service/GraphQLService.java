
package com.assignment.service;

import graphql.GraphQL;


public interface GraphQLService {
    
    public GraphQL getGraphQL();
    
}
