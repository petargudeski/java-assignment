
package com.assignment.service;

import com.assignment.exception.CustomFeedReaderException;

public interface FeedReaderService {
    
     public String getFeedData(String feedUrl, int pullSize) throws CustomFeedReaderException;
}
