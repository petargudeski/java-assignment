
package com.assignment.exception;


public class CustomFeedReaderException extends Exception {

	public CustomFeedReaderException(String message) {
		super(message);
	}

	public CustomFeedReaderException(String message, Exception e) {
		super(message, e);
	}

}

