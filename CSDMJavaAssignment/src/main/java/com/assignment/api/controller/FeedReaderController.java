package com.assignment.api.controller;

import com.assignment.model.Feed;
import com.assignment.repository.FeedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/feed")
@RestController
public class FeedReaderController {

    @Autowired
    FeedRepository feedRepository;

    @RequestMapping(value = "/getAllFeeds", method = RequestMethod.GET)
    public Page<Feed> getAllFeeds(@RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "100") int size) {
        PageRequest pageable = PageRequest.of(page, size);
        return feedRepository.findAll(pageable); 
    }
    
    @RequestMapping(value = "/getFeedById", method = RequestMethod.GET)
    public Feed getFeed(@RequestParam Long id) {
        return feedRepository.findOne(id);
    }
}
