package com.assignment.api.controller.graphql;

import com.assignment.service.GraphQLService;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/graphql/feed")
@RestController
public class FeedGraphqlController {

    @Autowired
    GraphQLService graphQLService;

    @PostMapping
    public ResponseEntity getAllFeed(@RequestBody String query) {
        ExecutionResult execute = graphQLService.getGraphQL().execute(query);
        if (execute.getData() != null) {
            return new ResponseEntity(execute, HttpStatus.OK);
        } else if (execute.getErrors().size() > 0) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
