
package com.assignment.controller;

import com.assignment.exception.CustomFeedReaderException;
import com.assignment.service.FeedReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
    
    @Autowired
    FeedReaderService feedReaderService;
    
    @GetMapping("/")
    public ModelAndView itemsNew(ModelAndView model) {
        model.setViewName("home");
        return model;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView saveStudent(ModelAndView model, @RequestParam String feedUrl, @RequestParam (defaultValue = "10") int size) 
            throws CustomFeedReaderException {
        
        String message = feedReaderService.getFeedData(feedUrl, size);
        
        model.addObject("message", message);
        model.setViewName("/home");
        return model;
    }
}
