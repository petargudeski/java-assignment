
package com.assignment.serviceImpl;

import com.assignment.service.GraphQLService;
import com.assignment.service.grapgql.dataFatcher.FeedDataFatcher;
import com.assignment.service.grapgql.dataFatcher.OneFeedDataFatcher;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraphQLServiceImpl implements GraphQLService{
    
    @Value("classpath:feed.graphql")
    Resource resource;
    
    private GraphQL graphQL;
    
    @Autowired
    FeedDataFatcher feedDataFatcher;
    
    @Autowired
    OneFeedDataFatcher oneFeedDataFatcher;
    
     // load schema at application start up
    @PostConstruct
    private void loadSchema() throws IOException {

        //loadDataIntoHSQL();
        
        // get the schema
        File schemaFile = resource.getFile();
        // parse schema
        TypeDefinitionRegistry typeRegistry;
        typeRegistry = new SchemaParser().parse(schemaFile);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }
    
    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("getAllFeeds", feedDataFatcher)
                        .dataFetcher("getOneFeed", oneFeedDataFatcher))
                .build();
    }

    @Override
    public GraphQL getGraphQL() {
        return graphQL;
    }
    
}
