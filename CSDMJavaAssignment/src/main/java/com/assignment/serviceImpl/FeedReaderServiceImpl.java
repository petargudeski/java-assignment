
package com.assignment.serviceImpl;

import com.assignment.exception.CustomFeedReaderException;
import com.assignment.model.Feed;
import com.assignment.repository.FeedRepository;
import com.assignment.service.FeedReaderService;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedReaderServiceImpl implements FeedReaderService {

    @Autowired
    FeedRepository feedRepository;
    
    @Override
    public String getFeedData(String url, int pullSize) throws CustomFeedReaderException{
        try {
            URL feedUrl = new URL(url);

            SyndFeedInput input = new SyndFeedInput();
            SyndFeed sf = input.build(new XmlReader(feedUrl));

            List entries = sf.getEntries();
            
            int size;
            String returnMessage = "";
            if(pullSize == 0){
                size = 10;
                returnMessage = "You have entered incorect size: " + pullSize +  ". Default size is 10.\n Successfuly pulled last " + size +" item/s from News Feed.";
            } else if(pullSize == entries.size()) {
                size = entries.size();
                return "You pulled all items from News Feed.";
            } else if(pullSize > entries.size()){
                size = 10;
                returnMessage = "You have entered incorect size: " + pullSize +  ". Default size is 10.\n Successfuly pulled last " + size +" item/s from News Feed.";
            } else {
                size = pullSize;
                returnMessage = "Successfuly pulled last " + size +" item/s from Feed.";
            }
      
            int counter = entries.size()-size;
            
            for(int i = entries.size()-1; i >= counter; i--) {
                SyndEntry entry = (SyndEntry) entries.get(i);
                
                Feed feed = new Feed();
                feed.setTitle(entry.getTitle());
                feed.setDescription(entry.getDescription().getValue());
                feed.setPublishedDate(entry.getPublishedDate());
                
                List<SyndEnclosure> enclosures = entry.getEnclosures();
                for (SyndEnclosure enclosure : enclosures) {
                    feed.setImage(enclosure.getUrl());
                }
                
                feedRepository.save(feed);
            }

            return returnMessage;
        } catch (MalformedURLException ex) {
            throw new CustomFeedReaderException("ERROR..MalformedURLException..." + ex.getMessage());
        } catch (IOException ex) {
            throw new CustomFeedReaderException("ERROR..IOException..." + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            throw new CustomFeedReaderException("ERROR..IllegalArgumentException..." + ex.getMessage());
        } catch (FeedException ex) {
            throw new CustomFeedReaderException("ERROR..FeedException..." + ex.getMessage());
        }
    }
    
}
