
package com.assignment.repository;

import com.assignment.model.Feed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedRepository extends JpaRepository<Feed, Long>{
    @Query("SELECT s FROM Feed s WHERE s.id = :id")
    Feed findOne(Long id);
}
