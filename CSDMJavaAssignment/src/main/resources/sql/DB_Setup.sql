CREATE DATABASE IF NOT EXISTS java_assignment CHARACTER SET utf8 COLLATE utf8_general_ci;

USE java_assignment;

CREATE TABLE IF NOT EXISTS Feed ( 
 id BIGINT(20) unsigned not null auto_increment,
 description LONGTEXT, 
 image VARCHAR(255),
 published_date DATETIME,
 title VARCHAR(255),
 primary key (id) 
 );