1. Run MySql script, the script is located 
https://bitbucket.org/petargudeski/java-assignment/src/master/CSDMJavaAssignment/src/main/resources/sql/
Copy the content from DB_Setup.sql and run in MySql server.

2. Clone or download source code from https://bitbucket.org/petargudeski/java-assignment/src/master/
3. Run the Java Spring Boot Application.
4. Go on http://localhost:8080/
5. Insert the feed url into "Insert feed url:" and insert number of items you want to save.(default value is 10. Items will be save in reverse mode)
 You can use graphQl or REST APIs to get the data from Data Base.
6. GraphQl examples:
	- POST http://localhost:8080/api/graphql/feed
		Body: {
  			{
                getAllFeeds{
                  id
    			  title
    			  description
    			  publishedDate
    			  image
                }
            }
	
	- POST http://localhost:8080/api/graphql/feed
		Body: {
 			    getOneFeed(id: "1") {
    			  id
    			  title
    			  description
    			  publishedDate
    			  image
  			    }
		      }

7. REST examples:
	- GET http://localhost:8080/api/feed/getFeedById?id=1
	- GET http://localhost:8080/api/feed/getAllFeeds